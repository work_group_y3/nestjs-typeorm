import { IsNotEmpty, Length, IsInt, Min } from 'class-validator';

export class CreateProductDto {
  @Length(4, 16)
  @IsNotEmpty()
  name: string;

  @IsInt()
  @Min(0)
  @IsNotEmpty()
  price: number;
}
